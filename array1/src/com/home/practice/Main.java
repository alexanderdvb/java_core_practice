package com.home.practice;

public class Main {

    public static void main(String[] args) {

        int[] integers = {3, 56, 1, 34, 6, 78, 44, 5, 11, 3, 8, 19, 23, 14};

        System.out.println("Array: ");
        for(int i = 0; i < integers.length;  i++){
            System.out.print(integers[i] + " ");
        }

        int min = integers[0];
        for(int i = 0; i < integers.length; i++){
            if(integers[i] < min){
                min = integers[i];
            }
        }
        System.out.println("\nMin element of array: " + min);

        int max = integers[0];
        for(int i = 0; i < integers.length; i++){
            if(integers[i] > max){
                max = integers[i];
            }
        }
        System.out.println("Max element of array: " + max);

        int sum = 0;
        for(int i = 0; i < integers.length; i++){
            sum += integers[i];
        }
        System.out.println("Sum = " + sum);
    }
}
