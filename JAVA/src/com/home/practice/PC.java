package com.home.practice;

public class PC {

  private int id;
  private String processor;
  private String motherboard;
  private String RAM;
  private String memory;
  private String GPU;
  private int price;


  public PC(int id, String processor, String motherboard, String RAM, String memory, String GPU, int price){

    this.id = id;
    this.processor = processor;
    this.motherboard = motherboard;
    this.RAM = RAM;
    this.memory = memory;
    this.GPU = GPU;
    this.price = price;

  }

  public int getId(){
    return id;
  }

  public void setId(int id){
    this.id = id;
  }

  public String getProcessor(){
    return processor;
  }

  public void setProcessor(String processor){
    this.processor = processor;
  }

  public String getMotherboard(){
    return motherboard;
  }

  public void setMotherboard(String motherboard){
    this.motherboard = motherboard;
  }

  public String getRAM(){
    return RAM;
  }

  public void setRAM(String RAM){
    this.RAM = RAM;
  }

  public String getMemory(){
    return memory;
  }

  public void setMemory(String memory){
    this.memory = memory;
  }

  public String getGPU(){
    return GPU;
  }

  public void setGPU(String GPU){
    this.GPU = GPU;
  }

  public int getPrice(){
    return price;
  }

  public void setPrice(int price){
    this.price = price;
  }

  @Override
  public String toString(){

    return "PC\n" +
            "id: " + id +
            "\n processor: " + processor +
            "\n motherboard: " + motherboard +
            "\n RAM: " + RAM +
            "\n memory: " + memory +
            "\n GPU: " + GPU +
            "\n price: " + price + " hrn ";

  }
}
