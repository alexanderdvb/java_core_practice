package com.home.practice;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] Array = {5, 52, 2, 3, 6, 7, 12, 45, 1, 2, 4, 2};

        System.out.println("Array: ");
        System.out.println(Arrays.toString(Array));

        System.out.println("Sorted Array: ");
        Arrays.sort(Array);
        System.out.println(Arrays.toString(Array));

        System.out.println("Filling Array with 1: ");
        Arrays.fill(Array,1);
        System.out.println(Arrays.toString(Array));

    }
}
