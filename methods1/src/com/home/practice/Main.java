package com.home.practice;

public class Main {

    public static void main(String[] args) {
        int a = 5;
        int b = 10;

        double c = 3.3;
        double d = 4.4;

        System.out.println("Initial variables: ");
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);
        System.out.println("d = " + d);

        System.out.println("Calculate sum of integers using method calculateSum(): ");
        int integerSum = calculateSum(a,b);
        System.out.println("Sum of integers is: " + integerSum);

        System.out.println("\n========================\n");

        System.out.println("Calculate sum of floating point numbers using method calculateSum(): ");
        double doubleSum = calculateSum(c,d);
        System.out.println("Sum of floating point numbers is: " + doubleSum);

        System.out.println("\n========================\n");


    }

    static int calculateSum(int a, int b) {
        return a + b;
    }

    static double calculateSum(double a, double b) {
        return a + b;
    }
}
