package com.home.practice;

public class Main {

    public static void main(String[] args) {
            Square square = new Square(4);
            Rectangle rectangle = new Rectangle(5, 7);

            int squareArea = square.calculateArea();
            int rectangleArea = rectangle.calculateArea();

            System.out.println("Square area: " + squareArea + " sq.cm");
            System.out.println("Rectangle area: " + rectangleArea + " sq.cm");

    }
}
